import * as authApiSpec from "@latency.gg/lgg-auth-oas";
import { minute } from "msecs";

export function mockCreateAuthorizationCode(
    server: authApiSpec.Server,
) {
    server.registerCreateAuthorizationCodeOperation(
        async incomingRequest => {
            const now = new Date();
            return {
                status: 201,
                parameters: {},
                entity() {
                    return {
                        expires: now.valueOf() + 10 * minute,
                        code: "AAAB",
                    };
                },
            };
        },
    );
}
