import * as metaApi from "@latency.gg/lgg-meta-api";
import { createHttpAgentSendReceive } from "@oas3/http-send-receive";
import { isAbortError, waitForAbort } from "abort-tools";
import { program } from "commander";
import delay from "delay";
import * as http from "http";
import { second } from "msecs";
import * as prom from "prom-client";
import { WaitGroup } from "useful-wait-group";
import * as application from "../application/index.js";

program.
    command("server").
    option("--port <number>", "Listen to port", Number, 8080).
    option(
        "--endpoint <url>",
        "endpoint of the service",
        v => new URL(v),
        new URL("http://localhost:8080"),
    ).
    option("--meta-port <number>", "Listen to meta-port", Number, 9090).
    option(
        "--meta-endpoint <url>",
        "endpoint of the meta-service",
        v => new URL(v),
        new URL("http://localhost:9090"),
    ).

    option(
        "--auth-api-endpoint <url>",
        "auth-api-endpoint",
        v => new URL(v),
        new URL("http://lgg-auth-api:8080"),
    ).

    option("--connect-timeout <msec>", "", Number, 10 * second).
    option("--idle-timeout <msec>", "", Number, 40 * second).
    option("--shutdown-delay <msec>", "", Number, 35 * second).

    option("--gitlab-client-id <id>", "", String, process.env.GITLAB_CLIENT_ID).
    option("--gitlab-client-secret <secret>", "", String, process.env.GITLAB_CLIENT_SECRET).

    option("--github-client-id <id>", "", String, process.env.GITHUB_CLIENT_ID).
    option("--github-client-secret <secret>", "", String, process.env.GITHUB_CLIENT_SECRET).

    option("--google-client-id <id>", "", String, process.env.GOOGLE_CLIENT_ID).
    option("--google-client-secret <secret>", "", String, process.env.GOOGLE_CLIENT_SECRET).

    action(programAction);

interface ActionOptions {
    port: number;
    metaPort: number;
    endpoint: URL;
    metaEndpoint: URL;

    authApiEndpoint: URL;

    shutdownDelay: number;
    connectTimeout: number;
    idleTimeout: number;

    gitlabClientId: string
    gitlabClientSecret: string

    githubClientId: string
    githubClientSecret: string

    googleClientId: string
    googleClientSecret: string
}
async function programAction(actionOptions: ActionOptions) {
    const {
        port,
        endpoint,
        metaPort,
        metaEndpoint,

        authApiEndpoint,

        connectTimeout,
        idleTimeout,
        shutdownDelay,

        gitlabClientId,
        gitlabClientSecret,

        githubClientId,
        githubClientSecret,

        googleClientId,
        googleClientSecret,
    } = actionOptions;

    console.log("starting...");

    process.
        on("uncaughtException", error => {
            console.error(error);
            process.exit(1);
        }).
        on("unhandledRejection", error => {
            console.error(error);
            process.exit(1);
        });

    const onWarn = (error: unknown) => {
        console.warn(error);
    };

    const livenessController = new AbortController();
    const readinessController = new AbortController();

    const promRegistry = new prom.Registry();
    prom.collectDefaultMetrics({ register: promRegistry });

    {
        /*
        When we receive a signal, the server should be not ready anymore, so
        we abort the readiness controller
        */
        const onSignal = (signal: NodeJS.Signals) => {
            console.log(signal);
            readinessController.abort();
        };
        process.addListener("SIGINT", onSignal);
        process.addListener("SIGTERM", onSignal);
    }

    const metaConfig: metaApi.Config = {
        endpoint: metaEndpoint,

        livenessController,
        readinessController,

        promRegistry,
    };
    const metaContext = metaApi.createContext(metaConfig);

    const config: application.Config = {
        endpoint,

        authApiEndpoint,

        gitlabClientId,
        gitlabClientSecret,

        githubClientId,
        githubClientSecret,

        googleClientId,
        googleClientSecret,

        abortController: livenessController,
        promRegistry,
        httpSendReceive: createHttpAgentSendReceive({
            connectTimeout,
            idleTimeout,
        }),
    };

    const applicationContext = application.createContext(config);

    /*
    We use the waitgroup to wait for requests to finish
    */
    const waitGroup = new WaitGroup();

    const applicationServer = application.createServer(
        applicationContext,
        onWarn,
    );
    applicationServer.use(
        (context, next) => waitGroup.with(() => next()),
    );

    const metaServer = metaApi.createServer(
        metaContext,
        onWarn,
    );
    metaServer.registerMiddleware(
        (route, request, next) => waitGroup.with(() => next(request)),
    );

    const applicationHttpServer = http.createServer(applicationServer.callback());
    const metaHttpServer = http.createServer(metaServer.asRequestListener({
        onError: onWarn,
    }));

    await new Promise<void>(resolve => applicationHttpServer.listen(
        port,
        () => resolve(),
    ));
    try {
        await new Promise<void>(resolve => metaHttpServer.listen(
            metaPort,
            () => resolve(),
        ));

        try {

            console.log("started");

            try {
                await waitForAbort(readinessController.signal, "application aborted");
            }
            catch (error) {
                if (!isAbortError(error)) throw error;
            }

            console.log("stopping...");

            /*
            wait a bit, usually for the load balancer to recognize this service as
            not ready and move traffic to another service.
            TODO: implement heartbeat so this will actually work!
            */
            await delay(shutdownDelay);

            /*
            wait for all requests to finish
            */
            await waitGroup.wait();

            /*
            abort the liveness controller to terminate all streaming responses!
            */
            livenessController.abort();

        }
        finally {
            await new Promise<void>((resolve, reject) => metaHttpServer.close(
                error => error ?
                    reject(error) :
                    resolve(),
            ));
        }
    }
    finally {
        await new Promise<void>((resolve, reject) => applicationHttpServer.close(
            error => error ?
                reject(error) :
                resolve(),
        ));
    }

    console.log("stopped");
}
