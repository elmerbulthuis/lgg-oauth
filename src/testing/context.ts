import * as authApiSpec from "@latency.gg/lgg-auth-oas";
import * as http from "http";
import * as prom from "prom-client";
import { Promisable } from "type-fest";
import * as application from "../application/index.js";

interface Context {
    endpoints: {
        application: URL,
        auth: URL,
    },
    servers: {
        application: application.Server,
        auth: authApiSpec.Server,
    },
    services: application.Services,
}

export async function withContext(
    job: (context: Context) => Promisable<void>,
) {
    const endpoints = {
        application: new URL("http://localhost:8080"),
        auth: new URL("http://localhost:9010"),
    };

    const abortController = new AbortController();
    const promRegistry = new prom.Registry();

    const applicationConfig: application.Config = {
        endpoint: endpoints.application,

        authApiEndpoint: endpoints.auth,

        gitlabClientId: process.env.GITLAB_CLIENT_ID ?? "",
        gitlabClientSecret: process.env.GITLAB_CLIENT_SECRET ?? "",

        githubClientId: process.env.GITHUB_CLIENT_ID ?? "",
        githubClientSecret: process.env.GITHUB_CLIENT_SECRET ?? "",

        googleClientId: process.env.GOOGLE_CLIENT_ID ?? "",
        googleClientSecret: process.env.GOOGLE_CLIENT_SECRET ?? "",

        abortController,
        promRegistry,
    };

    const onError = (error: unknown) => {
        if (!abortController.signal.aborted) {
            throw error;
        }
    };

    const applicationContext = application.createContext(
        applicationConfig,
    );

    const servers = {
        application: application.createServer(
            applicationContext,
            onError,
        ),
        auth: new authApiSpec.Server({
            baseUrl: endpoints.auth,
        }),
    };

    const httpServers = {
        application: http.createServer(servers.application.callback()),
        auth: http.createServer(servers.auth.asRequestListener({
            onError,
        })),
    };

    const context = {
        endpoints,
        servers,
        services: applicationContext.services,
    };

    const keys = Object.keys(httpServers) as Array<keyof typeof httpServers>;
    await Promise.all(
        keys.map(async key => new Promise<void>(resolve => httpServers[key].listen(
            endpoints[key].port,
            () => resolve(),
        ))),
    );

    try {
        await job(context);
    }
    finally {
        abortController.abort();

        await Promise.all(
            keys.map(async key => new Promise<void>(
                (resolve, reject) => httpServers[key].close(
                    error => error ?
                        reject(error) :
                        resolve(),
                )),
            ),
        );
    }

}
