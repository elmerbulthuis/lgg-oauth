import Router from "@koa/router";
import Koa from "koa";
import koaBody from "koa-body";
import * as providers from "../providers/index.js";
import * as routes from "../routes/index.js";
import { Context } from "./context.js";

export type Server = Koa;

export function createServer(
    context: Context,
    onError: (error: unknown) => void,
) {
    const server = new Koa();

    const router = new Router();

    router.get(
        "/authorize",
        routes.createAuthorizeRoute(context),
    );
    router.post(
        "/token",
        koaBody(),
        routes.createTokenRoute(context),
    );

    {
        const provider = new providers.GitlabProvider(
            new URL("/gitlab/callback", context.config.endpoint),
            context.config.gitlabClientId,
            context.config.gitlabClientSecret,
        );
        router.get(
            "/gitlab/authorize",
            routes.createProviderAuthorizeRoute(
                context,
                provider,
            ),
        );
        router.get(
            "/gitlab/callback",
            routes.createProviderCallbackRoute(
                context,
                provider,
            ),
        );
    }

    {
        const provider = new providers.GithubProvider(
            new URL("/github/callback", context.config.endpoint),
            context.config.githubClientId,
            context.config.githubClientSecret,
        );
        router.get(
            "/github/authorize",
            routes.createProviderAuthorizeRoute(
                context,
                provider,
            ),
        );
        router.get(
            "/github/callback",
            routes.createProviderCallbackRoute(
                context,
                provider,
            ),
        );
    }

    {
        const provider = new providers.GoogleProvider(
            new URL("/google/callback", context.config.endpoint),
            context.config.googleClientId,
            context.config.googleClientSecret,
        );
        router.get(
            "/google/authorize",
            routes.createProviderAuthorizeRoute(
                context,
                provider,
            ),
        );
        router.get(
            "/google/callback",
            routes.createProviderCallbackRoute(
                context,
                provider,
            ),
        );
    }

    server.
        use(async (context, next) => {
            try {
                await next();
            }
            catch (error) {
                onError(error);
            }
        }).
        use(router.routes()).
        use(router.allowedMethods());

    return server;
}
