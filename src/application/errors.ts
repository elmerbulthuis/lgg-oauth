import * as Koa from "koa";
import { Promisable } from "type-fest";

export abstract class Oauth2Error extends Error {
    abstract readonly errorCode: string
    abstract readonly status: number
}

export class Oauth2InvalidRequest extends Oauth2Error {
    readonly errorCode = "invalid_request"
    readonly status = 400
}

export class Oauth2InvalidClient extends Oauth2Error {
    readonly errorCode = "invalid_client"
    readonly status = 400
}

export class Oauth2InvalidGrant extends Oauth2Error {
    readonly errorCode = "invalid_grant"
    readonly status = 400
}

export class Oauth2UnsupportedGrantType extends Oauth2Error {
    readonly errorCode = "unsupported_grant_type"
    readonly status = 400
}

export class Oauth2UnsupportedResponseType extends Oauth2Error {
    readonly errorCode = "unsupported_response_type"
    readonly status = 400
}

export class Oauth2InvalidScope extends Oauth2Error {
    readonly errorCode = "invalid_scope"
    readonly status = 400
}

export class Oauth2UnauthorizedClient extends Oauth2Error {
    readonly errorCode = "unauthorized_client"
    readonly status = 401
}

export class Oauth2AccessDeniedClient extends Oauth2Error {
    readonly errorCode = "access_denied"
    readonly status = 403
}

export class Oauth2ServerError extends Oauth2Error {
    readonly errorCode = "server_error"
    readonly status = 500
}

export class Oauth2TemporarilyUnavailable extends Oauth2Error {
    readonly errorCode = "temporarily_unavailable"
    readonly status = 503
}

export async function withOauth2ErrorJson(
    context: Koa.Context,
    job: () => Promisable<void>,
): Promise<void> {
    try {
        await job();
    }
    catch (error) {
        let oauth2Error;
        if (error instanceof Oauth2Error) {
            oauth2Error = error;
        }
        else {
            oauth2Error = new Oauth2ServerError();
        }

        context.body = {
            error: oauth2Error.errorCode,
            error_description: oauth2Error.message,
        };
        context.status = oauth2Error.status;
    }
}

export async function withOauth2ErrorRedirect(
    context: Koa.Context,
    redirectUri: URL,
    job: () => Promisable<void>,
): Promise<void> {
    try {
        await job();
    }
    catch (error) {
        let oauth2Error;
        if (error instanceof Oauth2Error) {
            oauth2Error = error;
        }
        else {
            oauth2Error = new Oauth2ServerError();
        }

        context.body = {
            error: oauth2Error.errorCode,
            error_description: oauth2Error.message,
        };
        context.status = oauth2Error.status;
    }
}
