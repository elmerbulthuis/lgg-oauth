import { OidcProviderBase } from "./oidc-base.js";

export class GoogleProvider extends OidcProviderBase {
    readonly type = "google"
    readonly issuer = new URL("https://accounts.google.com")
}
