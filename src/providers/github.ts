import assert from "assert";
import fetch from "node-fetch";
import { OauthProviderBase } from "./oauth-base.js";

export class GithubProvider extends OauthProviderBase {
    readonly type = "github";
    readonly scope = ""

    async getAuthorizationEndpoint() {
        return new URL("https://github.com/login/oauth/authorize");
    }
    async getTokenEndpoint() {
        return new URL("https://github.com/login/oauth/access_token");
    }
    async getUserinfoEndpoint() {
        return new URL("https://api.github.com/user");
    }

    async getSubject(
        code: string,
    ) {
        const { type, callbackEndpoint, clientId, clientSecret } = this;

        const tokenEndpoint = await this.getTokenEndpoint();
        const userinfoEndpoint = await this.getUserinfoEndpoint();
        const redirectUri = new URL(`/${type}/callback`, callbackEndpoint);

        const requestBody = new URLSearchParams();
        requestBody.append("grant_type", "authorization_code");
        requestBody.append("code", code);
        requestBody.append("redirect_uri", redirectUri.toString());
        requestBody.append("client_id", clientId);
        requestBody.append("client_secret", clientSecret);

        const tokenResponse = await fetch(
            tokenEndpoint.toString(),
            {
                method: "POST",
                body: requestBody,
            },
        );
        assert.equal(tokenResponse.status, 200);
        const tokenResponseEntity = await tokenResponse.formData();

        console.log(
            Array.from(tokenResponseEntity.entries()),
        );
        const accessToken = tokenResponseEntity.get("access_token");

        const userinfoResponse = await fetch(
            userinfoEndpoint.toString(),
            {
                method: "GET",
                headers: {
                    "authorization": `token ${accessToken}`,
                },
            },
        );
        assert.equal(userinfoResponse.status, 200);
        const userInfoEntity = await userinfoResponse.json() as { node_id: string };
        const subject = userInfoEntity.node_id;

        return subject;
    }

}

